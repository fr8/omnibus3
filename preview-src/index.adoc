Freight Trust & Clearing
Author Name
:idprefix:
:idseparator: -
:!example-caption:
:!table-caption:
:page-pagination:

image:multirepo-ssg.svg[Multirepo SSG,200,float=right]
Platonem complectitur mediocritatem ea eos.
Ei nonumy deseruisse ius.
Mel id omnes verear.
Vis no velit audiam, sonet <<dependencies,praesent>> eum ne.
*Prompta eripuit* nec ad.
Integer diam enim, dignissim eget eros et, ultricies mattis odio.
Vestibulum consectetur nec urna a luctus.
Quisque pharetra tristique arcu fringilla dapibus.
Curabitur ut massa aliquam, cursus enim et, accumsan lectus.



:doctype: book

== Freight Trust Omnibus Documentation


image:https://gitlab.com/fr8/omnibus3/badges/master/pipeline.svg[link="https://gitlab.com/fr8/omnibus3/-/commits/master",title="pipeline status"]

Connecting the Worlds Supply Chain and Trade Finance Networks, together.


=== Rule Book

|===
| Section | Number

| General
| 100

| Governance
| 200

| Network & Clearing Participants
| 300

| Delivery and Settlement
| 400

| Obligations
| 500

| Suspension
| 600

| Reserved
| 700
|===

=== Supported EDI Formats

- ANSI 835 5010 X221
- ANSI 835 4010 X091
- ANSI 837 4010 X096
- ANSI 837 4010 X097
- ANSI 837 4010 X098
- ANSI 837 5010 X222
- ANSI 837 5010 X223


[discrete]
=== link:/erc-edi[$EDI]

ERC-20 $EDI Token +

[discrete]
=== link:/staking[Staking]

Staking Pools & How it Works +

[discrete]
=== link:/network[Connecting to the Network]

RPC Information +

[discrete]
=== link:/api-docs[API Docs]

API Documentation reference +

[NOTE]
====
Rulebook is under the SPD:NC-ND/2.5 License
====

[discrete]
=== Disclaimer & Copyright

{blank} + This work is copyrighted under the https://creativecommons.org/licenses/by-nc-nd/2.5/[Creative Commons NC-ND 2.5] License  +  +

*You are free to*: _Share & copy and redistribute the material in any medium or format_  +  + *Under the following terms*:  +  + _Attribution_: You must give appropriate credit, provide a link to the license, and indicate if changes were made.
You may do so in any reasonable manner, but not in any way that suggests the licensor endorses you or your use.
+  + _Non-Commercial_: You may not use the material for commercial purposes.
+  + _No-Derivatives_: If you remix, transform, or build upon the material, you may not distribute the modified material.
+  + _No additional restrictions_: You may not apply legal terms or technological measures that legally restrict others from doing anything the license permits.
+  + If you are seeking a Commerical license you may contact us at: link:mailto:admin@freighttrust.com[admin@freighttrust.com]

[discrete]
=== Corporate

USA Phone: (628) 222-5915  + 
Mailing Addresses: 1424 4th St Ste 214 PMB 1513
Santa Monica, CA 90401 -- 
(c) 2020 - FreightTrust & Clearing Corporation.
All Rights Reserved.
'
* [x] Telegram
* [x] Twitter
* [x] GitHub
* [x] FAQ

.Legal Entity Identifier 
[example]
254900C9UJMDGJ0ILK56


== Data Interchange 

Nominavi luptatum eos, an vim hinc philosophia intellegebat.
Lorem pertinacia `expetenda` et nec, [.underline]#wisi# illud [.line-through]#sonet# qui ea.
Eum an doctus <<liber-recusabo,maiestatis efficiantur>>.
Eu mea inani iriure.

[source,rust]
----
use edi::{loose_parse, parse};
#[test]
fn parse_empty_document() {
    assert!(parse("").is_err());
}

#[test]
fn missing_interchange() {
    let input = "GS*PO*SENDERGS*007326879*20020226*1534*1*X*004010~
ST*850*000000001~
BEG*****~
TEST*****~
ANY_AMOUNT_OF_CHARS_IS_OKAY_ALTHOUGH_ATYPICAL*****~
lowercase is chill***********************~
BEG*****~
BEG*****~
BEG*****~
BEG*****~
SE*10*000000001~
GE*1*1~";
    assert!(parse(input).is_err());
}
----



=== Some Code



----
.Transaction Pricing
[width="50%",cols=">s,^m,e",frame="topbot",options="header,footer"]
|==========================
|      2+|Columns 2 and 3
|1       |Segments  |Interchange
|GxdataZero       |4  |unit256
|GxdataNonZero       | 6,800  <1>  | 1,136
|GxminTransaction      |21,00 | 77,248 <2>
|footer 1|footer 2|footer 3
|==========================
----
<1> The cost per non-zero kilobyte of data
<2> Our reference EDI message 

Est in <<inline,reque>> homero principes, meis deleniti mediocrem ad has.
Altera atomorum his ex, has cu elitr melius propriae.
Eos suscipit scaevola at.

....
ISA*00*          *00*          *16*SENDER1        *1B*RECEIVER1      *071216*1406*U*00204*000000263*1*T*>~
GS*IN*SENDER1*RECEIVER1*20071216*1406*000000001*X*004010*~
ST*210*00000001~
B3**3410889*545930791T*TP*L*20071031*5165**20071029*035*NLMI~
C3*USD~
N9*AE*260451~
N9*OP*3410889~
G62*86*20071031~
R3*EXEM*B**AE***20071031*37905~
N1*SH*SIEMENS VDO S A DE C V - SIEMENS AUTOMOTIVE*94*99999~
N3*PERIFERICO SUR 7999D COMPLEJO IND~
N4*TLAQUEPAQUE*MX*99999*MX~
N1*CN*BRAMPTON ASSEMBLY - COLLINS AND AIKMAN*94*09126B~
N3*500 LAIRD ROAD*GUELPH PRODUCTS~
N4*GUELPH*ON*99999*CA~
N1*BT*BRAMPTON ASSEMBLY - MAIN*94*09126~
N3*2000 WILLIAMS PARKWAY EAST~
N4*BRAMPTON*ON*99999*CA~
N1*CA*NATIONAL LOGISTICS MANAGEMENT*94*45795~
N3*14320 JOY RD.~
N4*DETROIT*MI*48228*US~
N1*ZZ*EXPEDITORS/EMERY WORLDWIDE*94*37905~
N3*10881 LOWELL AVENUE~
N4*OVERLANDPARK*KS*66201*US~
N9*IK*545930791T~
N7**53456*********TL****5300*******53ST~
LX*1~
L5*1**2*Z~
L0*1*1499*FR*1499*G******L~
L1*1*51.65*FR*5165****400~
L3*2619*G***5165~
SE*30*00000001~
GE*1*000000001~
IEA*1*000000263~
....

[source,json]
....
{"Release":"00503","DocumentType":881,"TransactionSet":[{"ID":"ST","Min":0},{"ID":"BGN"},{"ID":"REF","Min":0,"Max":5},{"ID":"PER","Min":0,"Max":3},{"ID":"N1Loop1","Max":5,"Loop":[{"ID":"N1"},{"ID":"N2","Min":0},{"ID":"N3","Min":0,"Max":2},{"ID":"N4","Min":0}]},{"ID":"G01Loop1","Max":"unbounded","Loop":[{"ID":"G01"},{"ID":"QTY","Max":4},{"ID":"AMT","Max":2},{"ID":"G72Loop1","Min":0,"Max":10,"Loop":[{"ID":"G72"},{"ID":"G73","Min":0}]},{"ID":"N1Loop2","Max":"unbounded","Loop":[{"ID":"N1"},{"ID":"N2","Min":0},{"ID":"N3","Min":0,"Max":2},{"ID":"N4","Min":0},{"ID":"N9","Min":0},{"ID":"REFLoop1","Min":0,"Max":"unbounded","Loop":[{"ID":"REF"},{"ID":"QTY","Min":0,"Max":2},{"ID":"AMT","Min":0},{"ID":"G72","Min":0},{"ID":"LQLoop1","Min":0,"Max":"unbounded","Loop":[{"ID":"LQ"},{"ID":"AMT","Min":0},{"ID":"LIN","Min":0},{"ID":"QTY","Min":0},{"ID":"G72","Min":0},{"ID":"G73","Min":0,"Max":5}]}]}]}]},{"ID":"LXLoop1","Loop":[{"ID":"LX"},{"ID":"QTY"},{"ID":"AMT"}]},{"ID":"SE","Min":0}]}
....


==== $EDI & Wallets

Select menu:Account[Settings] to open your MetaMask Wallet.
Then menu:Network[Create Custom] and then hit kbd:[Enter].

.Having Issues?
****
We are here to help!
support@freight.zendesk.com

****

=== Roadmap & Details

Eu sed antiopam gloriatur.
Ea mea agam graeci philosophia.

* [x] Main Network Launch
* [x] Onboard Initial Nodes
* [x] Stress Testing
* [x] Bootstraping Phase
* [x] Token Launch
* [x] Token Listing
* [ ] Partnerships
* [ ] Audits
* [ ] Staking
* [ ] 2nd & 3rd Set of Nodes
* [ ] Governance

Vis veri graeci legimus ad.


.Library dependencies
[#dependencies%autowidth]
|===
|Library |Version

|besu
|^1.4

|tradedocs
|^11.0.0

|maidenlane
|^1.20.2

|central-dogma
|^0.4.3

|ptolemy
|^1.0.3

|omnibus
|^0.1.4
|===

Cum dicat putant ne.
Est in reque homero principes, meis deleniti mediocrem ad has.
Altera atomorum his ex, has cu elitr melius propriae.
Eos suscipit scaevola at.

[TIP]
This oughta do it!

Cum dicat putant ne.
Est in reque homero principes, meis deleniti mediocrem ad has.
Altera atomorum his ex, has cu elitr melius propriae.
Eos suscipit scaevola at.

[NOTE]
====
You've been down _this_ road before.
====

Cum dicat putant ne.
Est in reque homero principes, meis deleniti mediocrem ad has.
Altera atomorum his ex, has cu elitr melius propriae.
Eos suscipit scaevola at.


[CAUTION]
====
[#inline]#I wouldn't try that if I were you.#
====

[IMPORTANT]
====
Don't forget this step!
====

[discrete]
== Rulebook

Cum dicat putant ne.
Est in reque homero principes, meis deleniti mediocrem ad has.
Ex nam suas nemore dignissim, vel apeirian democritum et.

.Antora is a multi-repo documentation site generator
image::multirepo-ssg.svg[Multirepo SSG,250]

Make the switch today!

[#english+中文]
== English + 中文

Altera atomorum his ex, has cu elitr melius propriae.
Eos suscipit scaevola at.

[quote, 'Famous Person. Cum dicat putant ne.', 'Cum dicat putant ne. https://example.com[Famous Person Website]']
____
Lorem ipsum dolor sit amet, consectetur adipiscing elit.
Mauris eget leo nunc, nec tempus mi? Curabitur id nisl mi, ut vulputate urna.
Quisque porta facilisis tortor, vitae bibendum velit fringilla vitae! Lorem ipsum dolor sit amet, consectetur adipiscing elit.
Mauris eget leo nunc, nec tempus mi? Curabitur id nisl mi, ut vulputate urna.
Quisque porta facilisis tortor, vitae bibendum velit fringilla vitae!
____

== Lets Get Started

Begin reading the documentation...
